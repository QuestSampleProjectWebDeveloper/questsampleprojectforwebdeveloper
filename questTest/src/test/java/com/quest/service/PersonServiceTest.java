package com.quest.service;

import static org.junit.Assert.assertNotEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.quest.exception.ServiceException;
import com.quest.persistence.entity.PersonEntity;
import com.quest.questtest.QuestTestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuestTestApplication.class)
public class PersonServiceTest {
	
	@Autowired
	private PersonService personService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Test
	public void createPersonServiceTest() throws ServiceException {  
		logger.info("==================== start createPersonServiceTest ====================");
		
		Calendar c = Calendar.getInstance();
		c.set(1995, 0, 16);
		Date dob = c.getTime();
		PersonEntity personEntity = new PersonEntity("Saul", "0101010P", dob, "0868358421");
		personService.createUpdatePerson(personEntity);
		
		logger.info("==================== end createPersonServiceTest ====================");
	}
	
	@Test
	public void updatePersonServiceTest() throws ServiceException {  
		logger.info("==================== start updatePersonServiceTest ====================");
		
		PersonEntity findPersonById = personService.findPersonById("1111111T").get();
		findPersonById.setMobilePhone("0812834853");
		
		personService.createUpdatePerson(findPersonById);
		
		logger.info("==================== end updatePersonServiceTest ====================");
	}
	
	@Test(expected = ServiceException.class)
	public void updatePersonServiceTestException() throws ServiceException {  
		logger.info("==================== start updatePersonServiceTestException ====================");

		PersonEntity findPersonById = personService.findPersonById("1111111T").get();
		findPersonById.setMobilePhone("08128348537");
		
		personService.createUpdatePerson(findPersonById);
	}
	
	@Test(expected = ServiceException.class)
	public void createPersonServiceTestException() throws ServiceException {  
		logger.info("==================== start createPersonServiceTestException ====================");
		
		personService.createUpdatePerson(null);
	}

	@Test
	public void deletePersonServiceTest() throws ServiceException {
		logger.info("==================== start deletePersonServiceTest ====================");
		
		String id = "1221358K";
		personService.deletePerson(id);
			
		logger.info("==================== end deletePersonServiceTest ====================");
	}

	@Test(expected = ServiceException.class)
	public void deletePersonServiceTestException() throws ServiceException {
		logger.info("==================== start deletePersonServiceTest ====================");
		
		String id = "kkkkk";
		personService.deletePerson(id);
	}

	@Test
	public void getPersonsServiceTest() throws ServiceException {
		logger.info("==================== start getPersonsServiceTest ====================");
			List<PersonEntity> persons = (List<PersonEntity>) personService.getAllPersons();
			
			assertNotEquals("The result is 0 having data", true, persons.isEmpty());
				
			logger.info("==================== end getPersonsServiceTest ====================");
			
	}
}