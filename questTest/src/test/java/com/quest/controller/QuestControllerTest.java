package com.quest.controller;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.quest.orchestration.PersonOrchestration;
import com.quest.persistence.dto.PersonDto;
import com.quest.questtest.QuestTestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuestTestApplication.class)
public class QuestControllerTest {

	private MockMvc mockMvc;

	@Mock
	private PersonOrchestration personOrchestration;

	@InjectMocks
	private QuestControllerImpl questController;

	private PersonDto personDtoAux;
	private PersonDto personDtoAuxExists;
	private PersonDto badPersonDtoAux;

	private String deleteId;

	private String jsonPersonDtoAux;

	private List<PersonDto> list;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(questController).build();

		Calendar c = Calendar.getInstance();
		c.set(1995, 0, 1);
		Date time = c.getTime();

		Calendar c2 = Calendar.getInstance();
		c2.set(9999, 0, 1);
		Date time2 = c2.getTime();

		personDtoAux = new PersonDto();
		personDtoAux.setPpsnumber("1234567XX");
		personDtoAux.setFullname("Bob");
		personDtoAux.setBithday(time);
		personDtoAux.setPhone("0800000000");

		personDtoAuxExists = new PersonDto();
		personDtoAuxExists.setPpsnumber("1234567GG");
		personDtoAuxExists.setFullname("Bob");
		personDtoAuxExists.setBithday(time2);
		personDtoAuxExists.setPhone("0800000000");

		badPersonDtoAux = new PersonDto();
		badPersonDtoAux.setPpsnumber("1234567GG");
		badPersonDtoAux.setFullname("123456790==ERROR==123456790");
		badPersonDtoAux.setBithday(time2);
		badPersonDtoAux.setPhone("0100000000");

		deleteId = "1221358K";

		doNothing().when(personOrchestration).deletePerson(deleteId);
//		doThrow(illegalArgumentException).when(personOrchestration).deletePerson(null);

		list = new ArrayList<PersonDto>();
		list.add(badPersonDtoAux);
		list.add(personDtoAux);

		Mockito.when(personOrchestration.getAllPersons()).thenReturn(list);

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		jsonPersonDtoAux = ow.writeValueAsString(personDtoAux);
	}

	@Test
	public void createPersonTest() throws Exception {
		logger.info("==================== start createPersonTest ====================");
		doNothing().when(personOrchestration).createPerson(personDtoAux);

		ResultActions perform = mockMvc.perform(post("/person").accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON).content(jsonPersonDtoAux));
		MvcResult andReturn = perform.andExpect(status().isOk()).andReturn();

		assertTrue(andReturn.getResponse().getContentAsString().equals(String.format(
							"Se ha creado correctamente el registro con pps[%s]", personDtoAux.getPpsnumber())));

		logger.info("==================== end createPersonTest ====================");
	}

	@Test
	public void updatePersonControllerTest() throws Exception {
		logger.info("==================== start updatePersonControllerTest ====================");
		doNothing().when(personOrchestration).updatePerson(personDtoAux);

		ResultActions perform = mockMvc.perform(put("/person").accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON).content(jsonPersonDtoAux));
		MvcResult andReturn = perform.andExpect(status().isOk()).andReturn();

		assertTrue(andReturn.getResponse().getContentAsString().equals(String.format(
							"Se ha editado correctamente el registro con pps[%s]", personDtoAux.getPpsnumber())));

		logger.info("==================== end updatePersonControllerTest ====================");
	}

	@Test
	public void deletePersonControllerTest() throws Exception {
		logger.info("==================== start deletePersonControllerTest ====================");
		doNothing().when(personOrchestration).deletePerson(deleteId);

		ResultActions perform = mockMvc.perform(delete("/person/" + deleteId).accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON));
		MvcResult andReturn = perform.andExpect(status().isOk()).andReturn();

		String actual = andReturn.getResponse().getContentAsString();
		assertTrue(actual.equals(String.format("Se ha borrado correctamente el registro con pps[%s]",
							deleteId)));

		logger.info("==================== end deletePersonControllerTest ====================");
	}

	@Test
	public void getAllPersonsControllerTest() throws Exception {
		logger.info("==================== start getAllPersonsControllerTest ====================");

		Mockito.when(personOrchestration.getAllPersons()).thenReturn(list);

		ResultActions perform = mockMvc.perform(get("/person/all").accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON));
		MvcResult andReturn = perform.andExpect(status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();

		List<PersonDto> actual = mapper.readValue(andReturn.getResponse().getContentAsString(),
							new TypeReference<List<PersonDto>>() {
							});

		assertTrue(actual.get(0).getFullname().equals(badPersonDtoAux.getFullname()));
		assertTrue(actual.get(1).getFullname().equals(personDtoAux.getFullname()));

		verify(personOrchestration).getAllPersons();

		logger.info("==================== end getAllPersonsControllerTest ====================");
	}
}