package com.quest.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.quest.persistence.entity.PersonEntity;
import com.quest.persistence.repository.PersonRepository;
import com.quest.questtest.QuestTestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuestTestApplication.class)
public class PersonRepositoryTest {
	
	@Autowired
	private PersonRepository personRepository;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Test
	public void findByNameValid() {  
		logger.info("==================== start findByNameValid ====================");
		
		String name = "Kim Bauer";
		List<PersonEntity> findByName = personRepository.findByName(name);
		
		assertFalse("Empty list", findByName.isEmpty());
		assertEquals(name, findByName.get(0).getName());
		
		logger.info("==================== end findByNameValid ====================");
	}
	
	@Test
	public void findByNameNull() {  
		logger.info("==================== start findByNameNull ====================");

		List<PersonEntity> findByName = personRepository.findByName(null);
		
		assertNotNull("List cant be null", findByName);
		assertTrue("It must be an empty list", findByName.isEmpty());
		
		logger.info("==================== end findByNameNull ====================");
	}
	
	@Test
	public void findByNameNotFound() {  
		logger.info("==================== start findByNameNotFound ====================");
		
		List<PersonEntity> findByName = personRepository.findByName("999999");
		
		assertNotNull("List cant be null", findByName);
		assertTrue("It must be an empty list", findByName.isEmpty());
		
		logger.info("==================== end findByNameNotFound ====================");
	}
	
	@Test
	public void findByNameNotFound2() {  
		logger.info("==================== start findByNameNotFound2 ====================");
		
		List<PersonEntity> findByName = personRepository.findByName("Kim%");

		assertNotNull("List cant be null", findByName);
		assertTrue("It must be an empty list", findByName.isEmpty());
		
		logger.info("==================== end findByNameNotFound2 ====================");
	}
	
	@Test
	public void findByNameEmpty() {  
		logger.info("==================== start findByNameEmpty ====================");
		
		List<PersonEntity> findByName = personRepository.findByName("");

		assertNotNull("List cant be null", findByName);
		assertTrue("It must be an empty list", findByName.isEmpty());
		
		logger.info("==================== end findByNameEmpty ====================");
	}
	
}