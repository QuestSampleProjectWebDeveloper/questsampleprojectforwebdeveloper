package com.quest.orchestration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.quest.exception.OrchestrationException;
import com.quest.persistence.dto.PersonDto;
import com.quest.persistence.entity.PersonEntity;
import com.quest.questtest.QuestTestApplication;
import com.quest.service.PersonService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuestTestApplication.class)
public class PersonOrchestrationTest {
	
	@Mock
	private PersonService personService;

    @InjectMocks
    private PersonOrchestrationImpl personOrchestration;
    
    private PersonEntity personEntityAux;
    private PersonEntity badPersonEntityAux;
    
    private PersonDto personDtoAux;
    private PersonDto personDtoAuxExists;
    private PersonDto badPersonDtoAux;
    
    private String deleteId;
    private String deleteId2;
    private String deleteId3;
    
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	

    @Before
    public void setUp() throws Exception {
    	MockitoAnnotations.initMocks(this);
		
		Calendar c = Calendar.getInstance();
		c.set(1995, 0, 1);
		Date time = c.getTime();
		
		personEntityAux = new PersonEntity();
		personEntityAux.setPpsn("1234567XX");
		personEntityAux.setName("Bob");
		personEntityAux.setDob(time);
		personEntityAux.setMobilePhone("0800000000");
		
		Calendar c2 = Calendar.getInstance();
		c2.set(9999, 0, 1);
		Date time2 = c2.getTime();
		
		badPersonEntityAux = new PersonEntity();
		badPersonEntityAux.setPpsn("1234567GG");
		badPersonEntityAux.setName("123456790==ERROR==123456790");
		badPersonEntityAux.setDob(time2);
		badPersonEntityAux.setMobilePhone("0100000000");
		
		Optional<PersonEntity> optional = Optional.of(personEntityAux);

		personDtoAux= new PersonDto();
		personDtoAux.setPpsnumber("1234567XX");
		personDtoAux.setFullname("Bob");
		personDtoAux.setBithday(time);
		personDtoAux.setPhone("0800000000");
		
		personDtoAuxExists= new PersonDto();
		personDtoAuxExists.setPpsnumber("1234567GG");
		personDtoAuxExists.setFullname("Bob");
		personDtoAuxExists.setBithday(time2);
		personDtoAuxExists.setPhone("0800000000");
		
		badPersonDtoAux= new PersonDto();
		badPersonDtoAux.setPpsnumber("1234567GG");
		badPersonDtoAux.setFullname("123456790==ERROR==123456790");
		badPersonDtoAux.setBithday(time2);
		badPersonDtoAux.setPhone("0100000000");
		
		deleteId = "1221358K";
		deleteId2 = "5821312K";
		deleteId3 = "5887812K";
		
		IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
		
		doNothing().when(personService).createUpdatePerson(null);
		
		doNothing().when(personService).deletePerson(deleteId);
		doThrow(illegalArgumentException).when(personService).deletePerson(null);
		
		ArrayList<PersonEntity> list = new ArrayList<PersonEntity>();
		list.add(badPersonEntityAux);
		list.add(personEntityAux);
		
		Mockito.when(personService.getAllPersons()).thenReturn(list);
		Mockito.when(personService.findPersonById("22222")).thenReturn(Optional.empty());
		Mockito.when(personService.findPersonById(badPersonDtoAux.getPpsnumber())).thenReturn(optional);
		Mockito.when(personService.findPersonById(deleteId)).thenReturn(optional).thenReturn(Optional.empty());
		Mockito.when(personService.findPersonById(deleteId2)).thenReturn(Optional.empty());
		Mockito.when(personService.findPersonById(deleteId3)).thenReturn(optional);
		doThrow(illegalArgumentException).when(personService).findPersonById(null);
    }
    
    @Test
    public void findPersonByIdOrchestrationTest()throws OrchestrationException {  
    	logger.info("==================== start findPersonByIdOrchestrationTest ====================");
    	
    	PersonDto findPersonById = personOrchestration.findPersonById(badPersonDtoAux.getPpsnumber());
    	
    	assertEquals("Find by service doesnt work (DoB)", personDtoAux.getBithday(), findPersonById.getBithday()); 
    	assertEquals("Find by service doesnt work (PPS)", personDtoAux.getPpsnumber(), findPersonById.getPpsnumber());
    	
    	logger.info("==================== end findPersonByIdOrchestrationTest ====================");
    }
    
    @Test(expected = OrchestrationException.class)
    public void findPersonByIdOrchestrationTest2()throws OrchestrationException {  
    	logger.info("==================== start findPersonByIdOrchestrationTest2 ====================");
    	
    	personOrchestration.findPersonById("22222");
    }
    
    @Test(expected = OrchestrationException.class)
    public void findPersonByIdOrchestrationTestThows()throws OrchestrationException {  
    	logger.info("==================== start findPersonByIdOrchestrationTestThows ====================");
    	
    	personOrchestration.findPersonById(null);
    }

	@Test
	public void createPersonTest()throws OrchestrationException {  
		logger.info("==================== start createPersonTest ====================");
		
		personOrchestration.createPerson(personDtoAux);
		
		logger.info("==================== end createPersonTest ====================");
	}
	
	@Test(expected = OrchestrationException.class)
	public void createPersonTestThows()throws OrchestrationException {  
		logger.info("==================== start createPersonTestThows ====================");
		
		personOrchestration.createPerson(badPersonDtoAux);
	}
	
	@Test
	public void updatePersonOrchestrationTest()throws OrchestrationException {  
		logger.info("==================== start updatePersonOrchestrationTest ====================");
		
		personOrchestration.updatePerson(personDtoAuxExists);
		
		logger.info("==================== end updatePersonOrchestrationTest ====================");
	}
	
	@Test(expected = OrchestrationException.class)
	public void updatePersonOrchestrationTestThows()throws OrchestrationException {  
		logger.info("==================== start updatePersonOrchestrationTestThows ====================");
		
		personOrchestration.updatePerson(badPersonDtoAux);
	}

	@Test
	public void deletePersonOrchestrationTest() throws OrchestrationException {
		logger.info("==================== start deletePersonOrchestrationTest ====================");
		
		personOrchestration.deletePerson(deleteId);
			
		logger.info("==================== end deletePersonOrchestrationTest ====================");
	}

	@Test(expected = OrchestrationException.class)
	public void deletePersonOrchestrationTestException() throws OrchestrationException {
		logger.info("==================== start deletePersonOrchestrationTestException ====================");
		
		personOrchestration.deletePerson(deleteId2);
	}
	
	@Test(expected = OrchestrationException.class)
	public void deletePersonOrchestrationTestException2() throws OrchestrationException {
		logger.info("==================== start deletePersonOrchestrationTestException2 ====================");
		
		personOrchestration.deletePerson(deleteId3);
	}
	
	@Test(expected = OrchestrationException.class)
	public void deletePersonOrchestrationTestException3() throws OrchestrationException {
		logger.info("==================== start deletePersonOrchestrationTestException3 ====================");
		
		personOrchestration.deletePerson(null);
	}

	@Test
	public void getAllPersonsOrchestrationTest() {
		logger.info("==================== start getAllPersonsOrchestrationTest3 ====================");
		
		List<PersonDto> persons = (List<PersonDto>) personOrchestration.getAllPersons();
		
		assertNotEquals("The result is 0 having data", true, persons.isEmpty());
			
		logger.info("==================== end getAllPersonsOrchestrationTest ====================");
	}
}