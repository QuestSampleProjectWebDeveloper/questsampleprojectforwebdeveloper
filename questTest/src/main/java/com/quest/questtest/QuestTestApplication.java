package com.quest.questtest;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.quest"})
@EntityScan("com.quest.persistence.entity")
@EnableJpaRepositories("com.quest.persistence.repository")
@EnableAutoConfiguration
public class QuestTestApplication {

//	private static final Logger log = LoggerFactory.getLogger(QuestTestApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(QuestTestApplication.class, args);
	}
	

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    }

//	@Bean
//	public CommandLineRunner demo(PersonRepository repository) {
//		return (args) -> {
//
//			Calendar c1 = Calendar.getInstance();
//			c1.set(1980, 0, 1);
//			Date d1 = c1.getTime();
//			Calendar c2 = Calendar.getInstance();
//			c2.set(1980, 0, 1);
//			Date d2 = c2.getTime();
//			Calendar c3 = Calendar.getInstance();
//			c3.set(1980, 0, 1);
//			Date d3 = c3.getTime();
//			Calendar c4 = Calendar.getInstance();
//			c4.set(1980, 0, 1);
//			Date d4 = c4.getTime();
//			Calendar c5 = Calendar.getInstance();
//			c5.set(1980, 0, 1);
//			Date d5 = c5.getTime();
//			
//			// save a couple of persons
//			repository.save(new Person("Jack Bauer", "1234567T", d1, "0861234567"));
//			repository.save(new Person("Chloe O'Brian", "1111111T", d2, "0861234567"));
//			repository.save(new Person("Kim Bauer", "1221358K", d3, "0861234567"));
//			repository.save(new Person("David Palmer", "1881358K", d4, "0861234567"));
//			repository.save(new Person("Michelle Dessler", "1253558K", d5, "0861234567"));
//
//			// fetch all persons
//			log.info("Persons found with findAll():");
//			log.info("-------------------------------");
//			for (Person person : repository.findAll()) {
//				log.info(person.toString());
//			}
//			log.info("");
//
//			// fetch an individual persons by ID
//			repository.findById("1253558K")
//				.ifPresent(person -> {
//					log.info("Person found with findById(1L):");
//					log.info("--------------------------------");
//					log.info(person.toString());
//					log.info("");
//				});
//
//			// fetch persons by last name
//			log.info("Persons found with findByName('Bauer'):");
//			log.info("--------------------------------------------");
//			repository.findByName("Bauer").forEach(bauer -> {
//				log.info(bauer.toString());
//			});
//			// for (Person bauer : repository.findByName("Bauer")) {
//			// 	log.info(bauer.toString());
//			// }
//			log.info("");
//		};
//	}
}
