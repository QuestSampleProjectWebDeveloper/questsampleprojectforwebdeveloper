package com.quest.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quest.exception.ServiceException;
import com.quest.persistence.entity.PersonEntity;
import com.quest.persistence.repository.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService {
	
	@Autowired
	private PersonRepository personRepository;


	@Override
	public Optional<PersonEntity> findPersonById(String id) throws ServiceException {
		return personRepository.findById(id);
	}
	
	@Override
	public void createUpdatePerson(PersonEntity entity) throws ServiceException {
		try {
			personRepository.save(entity);
		} catch (Exception e) {
			throw new ServiceException("BD exception: PersonServiceImpl.createPerson");
		}
	}

	@Override
	public void deletePerson(String id) throws ServiceException {
		try {
			personRepository.deleteById(id);
		} catch (Exception e) {
			throw new ServiceException("BD exception: PersonServiceImpl.deletePerson");
		}
	}

	@Override
	public Iterable<PersonEntity> getAllPersons() {
		return personRepository.findAll();
	}
}