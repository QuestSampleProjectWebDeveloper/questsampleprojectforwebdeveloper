package com.quest.service;

import java.util.Optional;

import com.quest.exception.ServiceException;
import com.quest.persistence.entity.PersonEntity;

public interface PersonService {
	Optional<PersonEntity> findPersonById(String id) throws ServiceException;
	void createUpdatePerson(PersonEntity PersonEntity) throws ServiceException;
	void deletePerson(String id) throws ServiceException;
	Iterable<PersonEntity> getAllPersons();
}