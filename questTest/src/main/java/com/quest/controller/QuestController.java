package com.quest.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.quest.persistence.dto.PersonDto;

public interface QuestController {
	ResponseEntity<String> createPerson(PersonDto PersonEntity);
	ResponseEntity<String> updatePerson(PersonDto PersonEntity);
	ResponseEntity<String> deletePerson(String id);
	ResponseEntity<List<PersonDto>> getAllPersons();
}