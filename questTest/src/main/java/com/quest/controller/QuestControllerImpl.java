package com.quest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.quest.exception.OrchestrationException;
import com.quest.orchestration.PersonOrchestration;
import com.quest.persistence.dto.PersonDto;

@Controller
public class QuestControllerImpl implements QuestController{

	@Autowired
	private PersonOrchestration personOrchestration;

	@RequestMapping(value = "/person", method = RequestMethod.POST)
	public ResponseEntity<String> createPerson(@RequestBody PersonDto person) {
		try {
		
		personOrchestration.createPerson(person);

		String msg = String.format("Se ha creado correctamente el registro con pps[%s]", person.getPpsnumber());
		return ResponseEntity.ok(msg);
	} catch (OrchestrationException e) {
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
	}
	}

	@RequestMapping(value = "/person", method = RequestMethod.PUT)
	public ResponseEntity<String> updatePerson(@RequestBody PersonDto person) {
		try {
		personOrchestration.updatePerson(person);

		String msg = String.format("Se ha editado correctamente el registro con pps[%s]", person.getPpsnumber());
		return ResponseEntity.ok(msg);
	} catch (OrchestrationException e) {
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
	}
	}
	
	@RequestMapping(value = "/person/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletePerson(@PathVariable String id) {
		try {
			personOrchestration.deletePerson(id);

			String msg = String.format("Se ha borrado correctamente el registro con pps[%s]", id);
			return ResponseEntity.ok(msg);
		} catch (OrchestrationException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/person/all")
	public ResponseEntity<List<PersonDto>> getAllPersons() {
		return ResponseEntity.ok(personOrchestration.getAllPersons());
	}

}