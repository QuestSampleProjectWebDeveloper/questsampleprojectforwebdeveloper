package com.quest.persistence.dto;

import java.util.Date;

import org.dozer.Mapping;

public class PersonDto {
	
	/**
	 * The PPSN of the Person
	 */
    @Mapping("ppsn")
	private String ppsnumber;

	/**
	 * The name of the Person
	 */
    @Mapping("name")
    private String fullname;
    
    /**
     * The Day of Birdth of the Person
     */
    @Mapping("dob")
    private Date bithday;
    
    /**
     * The Mobile Phone of the Person
     */
    @Mapping("mobilePhone")
    private String phone;

    public PersonDto() {}

    public PersonDto(String fullname, String ppsnumber, Date bithday, String phone) {
        this.fullname = fullname;
        this.ppsnumber = ppsnumber;
        this.bithday = bithday;
        this.phone = phone;
    }
    
    public String getPpsnumber() {
		return ppsnumber;
	}

	public void setPpsnumber(String ppsnumber) {
		this.ppsnumber = ppsnumber;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Date getBithday() {
		return bithday;
	}

	public void setBithday(Date bithday) {
		this.bithday = bithday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
    public String toString() {
        return String.format(
                "Customer[Name='%s', PPS Number='%s', Day of Birdth= '%s', "
                + "Mobile Phone='%s']",
                fullname, ppsnumber, bithday.toString(), phone);
    }

}