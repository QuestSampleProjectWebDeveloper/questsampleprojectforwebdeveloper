package com.quest.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "person")
public class PersonEntity extends QuestEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The PPSN of the Person
	 */
	@Id
    @Column(name="ppsn")
	private String ppsn;

	/**
	 * The name of the Person
	 */
    @Column(name="name")
    private String name;
    
    /**
     * The Day of Birdth of the Person
     */
    @Column(name="dob")
    private Date dob;
    
    /**
     * The Mobile Phone of the Person
     */
    @Column(name="mobilePhone")
    private String mobilePhone;

    public PersonEntity() {}

    public PersonEntity(String name, String ppsn, Date dob, String mobilePhone) {
        this.name = name;
        this.ppsn = ppsn;
        this.dob = dob;
        this.mobilePhone = mobilePhone;
    }
    
    public String getPpsn() {
		return ppsn;
	}

	public void setPpsn(String ppsn) {
		this.ppsn = ppsn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@Override
    public String toString() {
        return String.format(
                "Customer[Name='%s', PPS Number='%s', Day of Birdth= '%s', "
                + "Mobile Phone='%s']",
                name, ppsn, dob.toString(), mobilePhone);
    }

}