package com.quest.persistence.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.quest.persistence.entity.PersonEntity;

public interface PersonRepository extends CrudRepository<PersonEntity, String> {

    List<PersonEntity> findByName(String name);
}