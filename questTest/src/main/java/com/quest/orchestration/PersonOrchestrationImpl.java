package com.quest.orchestration;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quest.exception.OrchestrationException;
import com.quest.exception.ServiceException;
import com.quest.exception.ValidateException;
import com.quest.persistence.dto.PersonDto;
import com.quest.persistence.entity.PersonEntity;
import com.quest.service.PersonService;

import javassist.NotFoundException;

@Service
public class PersonOrchestrationImpl implements PersonOrchestration {
	
	@Autowired
	private PersonService personService;
	
	private DozerBeanMapper mapper = new DozerBeanMapper();


	@Override
	public PersonDto findPersonById(String id) throws OrchestrationException {
		try {
			Optional<PersonEntity> findPersonById = Optional.empty();
			findPersonById = personService.findPersonById(id);
			return mapper.map(findPersonById.get(), PersonDto.class);
		} catch (Exception e) {
			throw new OrchestrationException(e.getMessage());
		}
	}
	
	@Override
	public void createPerson(PersonDto dto) throws OrchestrationException {
		PersonEntity entity = mapper.map(dto, PersonEntity.class);
		
		try {
			validatePerson(entity, true);
			personService.createUpdatePerson(entity);
		} catch (Exception e) {
			throw new OrchestrationException(e.getMessage());
		}
	}
	
	@Override
	public void updatePerson(PersonDto dto) throws OrchestrationException {
		PersonEntity entity = mapper.map(dto, PersonEntity.class);
		try {

			validatePerson(entity, false);
			personService.createUpdatePerson(entity);
		} catch (Exception e) {
			throw new OrchestrationException(e.getMessage());
		}
	}

	@Override
	public void deletePerson(String id) throws OrchestrationException {
		try {
			Optional<PersonEntity> findPersonById = personService.findPersonById(id);
			if(findPersonById.isPresent()) {
				personService.deletePerson(id);
			} else {
				throw new NotFoundException(String.format("Data with id [%s] not found", id));
			}
			
			Optional<PersonEntity> existsPerson = personService.findPersonById(id);
			if(existsPerson.isPresent()) {
				throw new ServiceException(String.format("The data with id [%s] it is not deleted", id));
			}
		} catch (Exception e) {
			throw new OrchestrationException(e.getMessage());
		}
	}

	@Override
	public List<PersonDto> getAllPersons() {
		Iterable<PersonEntity> allPersons = personService.getAllPersons();
		List<PersonDto> res = new ArrayList<PersonDto>();
		
		for( PersonEntity data : allPersons) {
			res.add(mapper.map(data, PersonDto.class));
		}
		return res;
	}
	
	private boolean validatePerson(PersonEntity person, boolean isSave) throws Exception {
		String name = person.getName();
		String ppsn = person.getPpsn();
		Date dob = person.getDob();
		
		String phoneNumber = person.getMobilePhone();
			StringBuilder msgErr = new StringBuilder();
		if(!isFullNameValid(name)) {
			msgErr.append("- The name is invalid /n");
		}
		if(personService.findPersonById(ppsn).isPresent() && isSave) {
			msgErr.append("- This pps number is duplicated /n");
		}
		if(!personService.findPersonById(ppsn).isPresent() && !isSave) {
			msgErr.append("- This pps number is doesn't exists /n");
		}
		
		
		ZoneId defaultZoneId = ZoneId.systemDefault();
        Instant instantDob = dob.toInstant();
        LocalDate localDateDob = instantDob.atZone(defaultZoneId).toLocalDate();
        Instant instantToday= new Date().toInstant();
        LocalDate localDateToday = instantToday.atZone(defaultZoneId).toLocalDate();
        
		Period p = Period.between(localDateDob, localDateToday);
		if(p.getYears() < 16 && dob.before(new Date())) {
			msgErr.append("- Date / Age not valid /n");
		}
		if(!phoneNumber.substring(0, 2).equals("08")) {
			msgErr.append("- Phone number must being with 08 prefix");
		}
		
		if(msgErr.length() > 0) {
			throw new ValidateException(msgErr.toString());
		} else {
			return true;
		}
	}

	private boolean isFullNameValid(String fullname) {
		return fullname.length() <= 25 && fullname.length() > 0 && StringUtils.isNotEmpty(fullname);
	}
}