package com.quest.orchestration;

import java.util.List;

import com.quest.exception.OrchestrationException;
import com.quest.persistence.dto.PersonDto;

public interface PersonOrchestration {
	PersonDto findPersonById(String id) throws OrchestrationException;
	void createPerson(PersonDto PersonEntity) throws OrchestrationException;
	void updatePerson(PersonDto PersonEntity) throws OrchestrationException;
	void deletePerson(String id) throws OrchestrationException;
	List<PersonDto> getAllPersons();
}